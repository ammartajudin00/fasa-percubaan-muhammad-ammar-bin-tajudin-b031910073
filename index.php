<html>
<body>

<form action="" method="post">
  Voltage (V): <input type="text" name="voltage"><br><br>
  Current (A): <input type="text" name="current"><br><br>
  Current Rate (kWh): <input type="text" name="rate"><br><br>
  <input type="submit" value="Calculate">
</form>

<?php
if ($_POST) {
  $voltage = $_POST["voltage"];
  $current = $_POST["current"];
  $rate = $_POST["rate"];
  $power = $voltage * $current;
  echo "Current Rate (RM): " . $rate . "<br>";
  echo "Power (kW): " . $power/1000 . "<br><br>";
  echo "<u>Energy (kWh) and Total (RM) per hour for 1 day:</u><br><br>";
  echo "<table border='1'>
  <tr>
  <th>Hour</th>
  <th>Energy (kWh)</th>
  <th>Total (RM)</th>
  </tr>";
  for ($hour = 1; $hour <= 24; $hour++) {
    $energy = $power * $hour / 1000;
    $total = $energy * $rate / 100;
    echo "<tr>";
    echo "<td>" . $hour . "</td>";
    echo "<td>" . $energy . "</td>";
    echo "<td>" . number_format( $total, 3 ) . "</td>";
    echo "</tr>";
  }
  echo "</table>";
}
?>

</body>
</html>